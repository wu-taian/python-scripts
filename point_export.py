import argparse
import csv
from influxdb import InfluxDBClient
from datetime import datetime, timedelta, timezone
import time
import pytz

point_header=['id','name','description','value','time']

######################## 百分比函数 ######################## 
def print_percentage(num1, num2):
    percentage = (num1 / num2) * 100
    print(f"\r进度:{percentage:.2f}%", end="")

######################## 数组函数 ######################## 
# 将日期按照partition进行分区，返回二维数组
def get_partitioned_arrays(args):
    partition = int(args.partition)
    # 示例数据
    date_list = date_range(args.start,args.end)
    partitioned_arrays = [date_list[i:i+partition] for i in range(0, len(date_list), partition)]
    return partitioned_arrays
# 获取id列表
def get_id_list(args):
    ids = [x for x in args.ids.split(',')]
    return ids


######################## influxdb函数 ######################## 
# 获取influxdb客户端
def get_influxdb_client(host, port, username, password):
    # 创建 InfluxDB 客户端
    client = InfluxDBClient(host=host, port=port, username=username, password=password, database='t5_net')
    return client

# 查询influxdb数据
def query_influxdb(client, start_time, end_time, id):
    sql = f'SELECT id,"name",description,first(value) as value,time  FROM "point" WHERE id=\'{id}\' and time>=\'{start_time}\' and time<=\'{end_time}\' group by time(10s) fill(none)'

    # print(sql)
    # 执行查询
    result = client.query(query=sql)
    # 处理查询结果
    data = []
    for points in result:
        for point in points:
            time = get_normal_time(point['time'])
            data.append({
                'id': point['id'],
                'name': point['name'],
                'description': point['description'],
                'value': point['value'],
                'time': time
            })
    return data


######################## 导出函数 ######################## 
# 导出测点
def export_point(client, dates, ids, output):
    result_list = []

    file_name = dates[0] + '_' + dates[-1]
    for date in dates:
        print(f'数据[{date}] 开始查询, start:', get_cur_time())
        start = date + ' 00:00:00'
        end = date + ' 23:59:59'
        index = 0
        for id in ids:
            result = query_influxdb(client,start,end,id)
            result_list.extend(result)    
            index=index+1
            print_percentage(index, len(ids))
        print(',    查询结束,   end:', get_cur_time())
    if not result_list:
        print(f'数据[{file_name}]查询为空.')
        return 0
    result_size = len(result_list)
    export_start_time = get_cur_time()
    print(f'数据[{file_name}]开始导出,start: {export_start_time}')
    export_to_csv(result_list, output, file_name)
    export_end_time = get_cur_time()
    result_size_str = str(result_size)
    print(f'数据[{file_name}]导出完成,  end: {export_end_time}, 总条数: {result_size_str}')
    return result_size

# 导出到csv
def export_to_csv(data, output, file_name):
    if not data:
        print('no data to export.')
        return
    file_path = output + file_name + '.csv'
    with open(file_path, 'w', newline='', encoding='utf-8') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=point_header)
        writer.writeheader()
        for row in data:
            writer.writerow(row)


######################## 时间函数 ######################## 
# 获取当前时间
def get_cur_time():
    now = datetime.now()
    # 格式化输出
    formatted_time = now.strftime("%Y-%m-%d %H:%M:%S")
    return formatted_time

# 将UTC格式的时间转换成正常时区的时间
def get_normal_time(input_datetime_utc):
    # 将输入的字符串转换为 datetime 对象
    dot_char = "."
    if dot_char in input_datetime_utc:
        # 2024-07-03T12:41:17.541Z
        input_datetime = datetime.strptime(input_datetime_utc, "%Y-%m-%dT%H:%M:%S.%fZ")
    else:
        # 2024-07-03T12:41:17Z
        input_datetime = datetime.strptime(input_datetime_utc, "%Y-%m-%dT%H:%M:%SZ")
    # 将 datetime 对象转换为 UTC 时间
    utc_datetime = input_datetime.replace(tzinfo=pytz.utc)

    # 将 UTC datetime 对象转换为北京时间
    beijing_tz = pytz.timezone('Asia/Shanghai')
    beijing_datetime = utc_datetime.astimezone(beijing_tz)

    # 将北京时间 datetime 对象转换为字符串
    beijing_datetime_str = beijing_datetime.strftime("%Y-%m-%d %H:%M:%S")
    return beijing_datetime_str

# 获取日期列表，从开始日期到结束日期
def date_range(start_date, end_date):
    # 将输入的字符串转换为datetime对象
    start_date = datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.strptime(end_date, "%Y-%m-%d")
    
    # 初始化日期列表
    date_list = []
    
    # 遍历从开始日期到结束日期的每一天
    current_date = start_date
    while current_date <= end_date:
        date_list.append(current_date.strftime("%Y-%m-%d"))
        current_date += timedelta(days=1)
    return date_list

# 主函数
def main():
    parser = argparse.ArgumentParser(description="export csv file.")
    parser.add_argument('-host', '--host', required=True, help='please enter influxdb host.')
    parser.add_argument('-port', '--port', required=True, help='please enter influxdb post.')
    parser.add_argument('-username', '--username', required=True, help='please enter influxdb username.')
    parser.add_argument('-password', '--password', required=True, help='please enter influxdb password.')
    parser.add_argument('-start', '--start', required=True, help='please enter start date of data.')
    parser.add_argument('-end', '--end', required=True, help='please enter end date of data.')
    parser.add_argument('-partition', '--partition', required=True, help='please enter partition of file.')
    parser.add_argument('-output', '--output', required=True, help='please enter output of file.')
    parser.add_argument('-ids', '--ids', required=True, help='please enter output of file.')

    args = parser.parse_args()
    # 获取分区后日期二维数组
    partitioned_dates = get_partitioned_arrays(args)
    if not partitioned_dates:
        print("invalid time format.")
        return
    # 获取Id
    ids = get_id_list(args)
    if not ids:
        print("id is empty.")
        return
    total_size = 0
    start_time = time.time()
    try:
        # 获取客户端连接
        client = get_influxdb_client(args.host,args.port,args.username,args.password)
        # 每个分区进行批量导出
        for dates in partitioned_dates:
            cur_size = export_point(client,dates,ids,args.output)
            total_size = total_size + cur_size
    except Exception as e:
        print(f"export create a error: {e}")
    finally:
        end_time = time.time()
        # 总条数
        print("total size is: " + str(total_size))
        print("total cost is: " + str(end_time - start_time) + "s")
        # 关闭客户端连接
        client.close()

if __name__ == '__main__':
    main()